package com.companyname.projectname.bizmoduler.sample;

import com.agileai.hotweb.bizmoduler.core.BaseService;

public class DemoBizManageImpl
        extends BaseService
        implements DemoBizManage {

	@Override
	public String sayHi(String theGuyName) {
		String result = "Hello " + theGuyName + " !";
		return result;
	}
	
}
