package com.agileai.miscdp.hotweb.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.agileai.miscdp.hotweb.domain.DeployResource;

public class DeployDialog extends Dialog {
	private Table table;

	private List<DeployResource> selectedDeployFilesList = new ArrayList<DeployResource>();
	private List<DeployResource> inputDeployFilesList = new ArrayList<DeployResource>();	
	private CheckboxTableViewer checkboxTableViewer = null;
	
	private String deployType = null;
	private Button moduleRadioButton;
	private Button appFileRadioButton;
	private Button applicationRadioButton;
	private Button reloadButton;
	private boolean containReloadResouce = false;
	
	private List<DeployResource> selectedDeployResources = new ArrayList<DeployResource>();
	
	private boolean needReload = false;
	
	public DeployDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.verticalSpacing = 3;
		gridLayout.marginHeight = 5;
		gridLayout.horizontalSpacing = 2;
		
		Composite radioComposite = new Composite(container, SWT.NONE);
		radioComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3));
		GridLayout gl_radioComposite = new GridLayout(3, false);
		gl_radioComposite.marginWidth = 2;
		gl_radioComposite.marginHeight = 2;
		gl_radioComposite.horizontalSpacing = 2;
		radioComposite.setLayout(gl_radioComposite);
		
		moduleRadioButton = new Button(radioComposite, SWT.RADIO);
		moduleRadioButton.setEnabled(false);
		moduleRadioButton.setText(Messages.getString("DeployDialog.moduleLabel"));
		
		appFileRadioButton = new Button(radioComposite, SWT.RADIO);
		appFileRadioButton.setEnabled(false);
		appFileRadioButton.setBounds(0, 0, 97, 17);
		appFileRadioButton.setText(Messages.getString("DeployDialog.resourceLabel"));
		
		applicationRadioButton = new Button(radioComposite, SWT.RADIO);
		applicationRadioButton.setEnabled(false);
		applicationRadioButton.setBounds(0, 0, 97, 17);
		applicationRadioButton.setText(Messages.getString("DeployDialog.applicationLabel"));
		
		Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_1.setLayout(new GridLayout(1, false));
		
		this.checkboxTableViewer = CheckboxTableViewer.newCheckList(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		table = checkboxTableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		final TableColumn nameColumnTableColumn = new TableColumn(table, SWT.NONE);
		nameColumnTableColumn.setWidth(210);
		nameColumnTableColumn.setText("名称");

		final TableColumn typeColumnTableColumn = new TableColumn(table, SWT.NONE);
		typeColumnTableColumn.setWidth(83);
		typeColumnTableColumn.setText("类型");
		
		checkboxTableViewer.setLabelProvider(new DeployResourceLabelProvider());
		checkboxTableViewer.setContentProvider(new ArrayContentProvider());
		
		final CellEditor[] filesCellEditor = new CellEditor[table.getColumnCount()]; 
		filesCellEditor[0] = new TextCellEditor(table);
		
		checkboxTableViewer.setCellEditors(filesCellEditor);
		checkboxTableViewer.setColumnProperties(DeployResource.columnProperties); 
		
		checkboxTableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				return false;
			}
			public Object getValue(Object element, String property) {
				DeployResource resource = (DeployResource)element; 
				if ("name".equals(property)){
					return resource.getFileName();
				}
				else if ("type".equals(property)){
					return resource.getFileType();
				}
				return null;
			}
			public void modify(Object element, String property, Object value) {
				
			}
		});
		
		checkboxTableViewer.setInput(inputDeployFilesList);
		checkboxTableViewer.setCheckedElements(selectedDeployFilesList.toArray(new DeployResource[0]));
		
		Composite extendArea = new Composite(container, SWT.NONE);
		extendArea.setLayout(new GridLayout(1, false));
		
		reloadButton = new Button(extendArea, SWT.CHECK);

		this.initValues();
		return container;
	}

	private void initValues(){
		if (this.deployType.equals(DeployResource.DeployTypes.Application)){
			this.applicationRadioButton.setSelection(true);
			this.moduleRadioButton.setSelection(false);
			this.appFileRadioButton.setSelection(false);
			
			reloadButton.setSelection(true);
			reloadButton.setText("重启应用");
		}
		else if (this.deployType.equals(DeployResource.DeployTypes.Modules)){
			this.moduleRadioButton.setSelection(true);
			this.appFileRadioButton.setSelection(false);
			this.applicationRadioButton.setSelection(false);
			
			reloadButton.setText("重新加载模块");
			reloadButton.setSelection(true);
		}
		else if (this.deployType.equals(DeployResource.DeployTypes.AppFiles)){
			this.appFileRadioButton.setSelection(true);
			this.moduleRadioButton.setSelection(false);
			this.applicationRadioButton.setSelection(false);
			
			reloadButton.setText("重启应用");
			
			if (containReloadResouce){
				reloadButton.setSelection(true);
			}
		}
	}
	
	@Override
	protected void okPressed() {
		this.needReload = this.reloadButton.getSelection();
		
		Object[] objects = checkboxTableViewer.getCheckedElements();
		if (objects != null){
			for (int i=0;i < objects.length;i++){
				DeployResource deployResource = (DeployResource)objects[i];
				this.selectedDeployResources.add(deployResource);
			}
		}
		
		super.okPressed();
	}

	public boolean isNeedReload() {
		return needReload;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Messages.getString("DeployDialog.title"));
	}	
	
	public List<DeployResource> getSelectedDeployFilesList() {
		return selectedDeployFilesList;
	}

	public void setSelectedDeployFilesList(List<DeployResource> selectedDeployFilesList) {
		this.selectedDeployFilesList = selectedDeployFilesList;
	}

	public List<DeployResource> getInputDeployFilesList() {
		return inputDeployFilesList;
	}

	public void setInputDeployFilesList(List<DeployResource> inputDeployFilesList) {
		this.inputDeployFilesList = inputDeployFilesList;
	}
	
	public String getDeployType() {
		return deployType;
	}

	public void setDeployType(String deployType) {
		this.deployType = deployType;
	}

	@Override
	protected Point getInitialSize() {
		return new Point(343, 477);
	}
	public Button getModuleRadioButton() {
		return moduleRadioButton;
	}
	public Button getAppFileRadioButton() {
		return appFileRadioButton;
	}
	public Button getApplicationRadioButton() {
		return applicationRadioButton;
	}
	public Button getReloadButton() {
		return reloadButton;
	}
	public void setContainReloadResouce(boolean containReloadResouce) {
		this.containReloadResouce = containReloadResouce;
	}
	public List<DeployResource> getSelectedDeployResources() {
		return selectedDeployResources;
	}
}

class DeployResourceLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof DeployResource) {
			DeployResource p = (DeployResource) element;
			if (columnIndex == 0) {
				return p.getFileName();
			}
			else if (columnIndex == 1){
				return p.getFileType();
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
