package com.agileai.miscdp.hotweb.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;

public class FieldSelectDialog extends Dialog {
	private String selectedField = null;
	
	private String[] fields = null; 
	private Text targetText = null;
	private List fieldsList = null;
	
	private BaseModelEditor modelEditor = null;
	
	public FieldSelectDialog(BaseModelEditor modelEditor,String[] fields,Text targetText) {
		super(modelEditor.getSite().getShell());
		this.fields = fields;
		this.targetText = targetText;
		this.modelEditor = modelEditor;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));
		
		this.fieldsList = new List(container, SWT.BORDER);
		fieldsList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}
		});
		fieldsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		fieldsList.setItems(fields);

		return container;
	}
	
	public String getSelectedField() {
		return selectedField;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {
		String[] selects = this.fieldsList.getSelection();
		if (selects != null){
			this.selectedField = selects[0];
			this.targetText.setText(this.selectedField);
			this.modelEditor.setModified(true);
			this.modelEditor.fireDirty();
		}
		super.okPressed();
	}

	@Override
	protected Point getInitialSize() {
		return new Point(338, 443);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("选择字段");
	}
}
