﻿package com.agileai.miscdp.hotweb.ui.views;

import java.util.ArrayList;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IEditorPart;
/**
 * 树对象
 */
public class TreeObject implements IAdaptable{
	private String id = "0";
	private String name;
	private String projectName = null;
	private TreeObject parent;
	private String nodeType = null;
	private String funcType = null;
	private String funcTypeName = null;
	private String funcSubPkg = null;
	
	private IEditorPart editor = null;
	
	private ArrayList<TreeObject> children;
	
	public static final String NODE_LEAF = "BaseFuncModel";
	public static final String NODE_COMPOSITE = "FuncModule";
	public static final String NODE_PROJECT = "Project";
	
	public TreeObject(String id,String name) {
		this.name = name;
		this.id = id;
		children = new ArrayList<TreeObject>();
	}
	public String getFuncType() {
		return funcType;
	}
	public void setFuncType(String funcType) {
		this.funcType = funcType;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setParent(TreeObject parent) {
		this.parent = parent;
	}
	public TreeObject getParent() {
		return parent;
	}
	public String toString() {
		return getName();
	}
	public String getId() {
		return id;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {
		return null;
	}
	public String getNodeType() {
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public void addChild(TreeObject child) {
		children.add(child);
		child.setParent(this);
	}
	public void removeChild(TreeObject child) {
		children.remove(child);
		child.setParent(null);
	}
	public TreeObject [] getChildren() {
		return (TreeObject [])children.toArray(new TreeObject[children.size()]);
	}
	public boolean hasChildren() {
		return children!= null && children.size()>0;
	}
	public IEditorPart getEditor() {
		return editor;
	}
	public void setEditor(IEditorPart editor) {
		this.editor = editor;
	}
	public boolean isProjectNode(){
		return NODE_PROJECT.equals(this.nodeType);
	}
	public String getFuncTypeName() {
		return funcTypeName;
	}
	public void setFuncTypeName(String funcTypeName) {
		this.funcTypeName = funcTypeName;
	}
	public String getFuncSubPkg() {
		return funcSubPkg;
	}
	public void setFuncSubPkg(String funcSubPkg) {
		this.funcSubPkg = funcSubPkg;
	}
}
