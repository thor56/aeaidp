package com.agileai.miscdp.hotweb.ui.editors.treecontent;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
import com.agileai.miscdp.hotweb.ui.celleditors.CompositeProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.TableViewerKeyboardSupporter;
import com.agileai.miscdp.hotweb.ui.celleditors.ValidationEditor;
import com.agileai.miscdp.hotweb.ui.editors.AddOrDelOperation;
import com.agileai.miscdp.hotweb.ui.editors.UpOrDownOperation;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.ListUtil;
/**
 * 明细Jsp配置页面
 */
public class TACTreeEditArea extends Composite{
	private TableViewer tableViewer;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Table table;
	private TreeContentFuncModel funcModel= null;
	private TreeContentModelEditor modelEditor = null;
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public TACTreeEditArea(Composite parent,TreeContentModelEditor modelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();
		this.createContent();
	}
	private void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);
		
		final Label pageTitleLabel_1 = new Label(this,SWT.NONE);
		pageTitleLabel_1.setText("表单元素");

		tableViewer = new TableViewer(this,SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		table = tableViewer.getTable();
		final GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.widthHint = 523;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		final TableColumn labelColumn = new TableColumn(table, SWT.NONE);
		labelColumn.setWidth(120);
		labelColumn.setText("Label");

		final TableColumn codeColumn = new TableColumn(table, SWT.NONE);
		codeColumn.setWidth(120);
		codeColumn.setText("Code");

		final TableColumn tagTypeColumn = new TableColumn(table, SWT.NONE);
		tagTypeColumn.setWidth(70);
		tagTypeColumn.setText("TagType");

		final TableColumn dataTypeColumn = new TableColumn(table, SWT.NONE);
		dataTypeColumn.setWidth(70);
		dataTypeColumn.setText("DataType");

		final TableColumn formatColumn = new TableColumn(table, SWT.NONE);
		formatColumn.setWidth(120);
		formatColumn.setText("Format");

		final TableColumn validationColumn = new TableColumn(table, SWT.NONE);
		validationColumn.setWidth(180);
		validationColumn.setText("Validations");
		tableViewer.setLabelProvider(new FormFieldLabelProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		final CellEditor[] pageParamEditors = new CellEditor[table.getColumnCount()]; 
		pageParamEditors[0] = new TextCellEditor(table,0);
		pageParamEditors[1] = new CompositeProviderEditor(modelEditor,tableViewer,table);
		pageParamEditors[2] = new ComboBoxCellEditor(table,PageFormField.simpleTagTypes, SWT.READ_ONLY); 
		pageParamEditors[3] = new ComboBoxCellEditor(table,PageFormField.dataTypes, SWT.READ_ONLY);
		pageParamEditors[4] = new ComboBoxCellEditor(table,PageFormField.formats, SWT.READ_ONLY);
		pageParamEditors[5] = new ValidationEditor(modelEditor,tableViewer,table);
		
		tableViewer.setCellEditors(pageParamEditors);
		tableViewer.setColumnProperties(PageFormField.columnProperties); 
		
		TableViewerKeyboardSupporter tableViewerSupporter = new TableViewerKeyboardSupporter(tableViewer); 
		tableViewerSupporter.startSupport();
		tableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				return true;
			}
			@SuppressWarnings("rawtypes")
			public Object getValue(Object element, String property) {
				PageFormField pageParameter = (PageFormField) element; 
				if (PageFormField.LABEL.equals(property)){
					return pageParameter.getLabel();
				}
				else if (PageFormField.CODE.equals(property)){
					return pageParameter.getCode();
				}
				else if (PageFormField.DATATYPE.equals(property)){
					return getDataTypeIndex(pageParameter.getDataType());
				}
				else if (PageFormField.FORMAT.equals(property)){
					return getFormatIndex(pageParameter.getFormat());
				}
				else if (PageFormField.TAGTYPE.equals(property)){
					return getTagTypeIndex(pageParameter.getTagType());
				}
				else if (PageFormField.VALIDATIONS.equals(property)){
					List validations = pageParameter.getValidations();
					return MiscdpUtil.getValidation(validations);					
				}
				return null;
			}
			private int getFormatIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageFormField.formats.length;i++){
					if (PageFormField.formats[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getTagTypeIndex(String tagType){
				int result = 0;
				for (int i=0;i < PageFormField.simpleTagTypes.length;i++){
					if (PageFormField.simpleTagTypes[i].equals(tagType)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getDataTypeIndex(String dataType){
				int result = 0;
				for (int i=0;i < PageFormField.dataTypes.length;i++){
					if (PageFormField.dataTypes[i].equals(dataType)){
						result = i;
						break;
					}
				}
				return result;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				PageFormField pageParameter = (PageFormField) item.getData();
				boolean changed = false;
				if (PageFormField.LABEL.equals(property)){
					if (!String.valueOf(value).equals(pageParameter.getLabel())){
						pageParameter.setLabel(String.valueOf(value));
						changed = true;
					}
				}
				else if (PageFormField.DATATYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.dataTypes[index].equals(pageParameter.getDataType())){
						pageParameter.setDataType(PageFormField.dataTypes[index]);
						changed = true;
					}
				}
				else if (PageFormField.TAGTYPE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.simpleTagTypes[index].equals(pageParameter.getTagType())){
						pageParameter.setTagType(PageFormField.simpleTagTypes[index]);
						changed = true;
					}
				}
				else if (PageFormField.FORMAT.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !PageFormField.formats[index].equals(pageParameter.getFormat())){
						pageParameter.setFormat(PageFormField.formats[index]);
						changed = true;
					}
				}
				if (changed){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					tableViewer.refresh();					
				}
			}
		});
		
		List<PageFormField> pageFormFields = this.funcModel.getTreeEditFormFields();
		tableViewer.setInput(pageFormFields);
		
		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.verticalSpacing = 0;
		gridLayout_1.marginWidth = 0;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.horizontalSpacing = 0;
		composite.setLayout(gridLayout_1);

		final Button addrowButton = new Button(composite, SWT.NONE);
		addrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().addRow(tableViewer,PageFormField.class);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		addrowButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addrowButton.setText("添加");

		final Button deleteButton = new Button(composite, SWT.NONE);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			@SuppressWarnings("rawtypes")
			public void widgetSelected(SelectionEvent e) {
				Combo tableNameCombo = ((TreeContentModelEditor)modelEditor).getBasicConfigPage().getTableNameCombo();
				String tableName = tableNameCombo.getText();
				String projectName = funcModel.getProjectName();
				String [] primaryKeys = DBManager.getInstance(projectName).getPrimaryKeys(tableName);
				List pkList = ListUtil.arrayToList(primaryKeys);
				int selectIndex = tableViewer.getTable().getSelectionIndex();
				if (selectIndex != -1){
					List inputList = (List)tableViewer.getInput();
					PageFormField pageFormField = (PageFormField)inputList.get(selectIndex);
					String columnCode = pageFormField.getCode();
					if (pkList.contains(columnCode)){
						Shell shell = modelEditor.getSite().getShell();
						MessageDialog.openInformation(shell, "消息提示","该字段是主键不能删除!");
						return;
					}
				}
				
				new AddOrDelOperation().delRow(tableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteButton.setText("删除");

		final Button upButton = new Button(composite, SWT.NONE);
		upButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		upButton.setText("上移");

		final Button downButton = new Button(composite, SWT.NONE);
		downButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		downButton.setText("下移");
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}

	public TreeContentFuncModel buildConfig() {
		return this.funcModel;
	}
	public TableViewer getTableViewer() {
		return tableViewer;
	}
}
class FormFieldLabelProvider extends LabelProvider implements ITableLabelProvider {
	@SuppressWarnings("rawtypes")
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof PageFormField) {
			PageFormField p = (PageFormField) element;
			if (columnIndex == 0) {
				return p.getLabel();
			} else if (columnIndex == 1) {
				return p.getCode();
			} else if (columnIndex == 2) {
				return p.getTagType();
			} else if (columnIndex == 3) {
				return p.getDataType();			
			} else if (columnIndex == 4) {
				return p.getFormat();				
			} else if (columnIndex == 5) {
				List validations = p.getValidations();
				return MiscdpUtil.getValidation(validations);
			}
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
