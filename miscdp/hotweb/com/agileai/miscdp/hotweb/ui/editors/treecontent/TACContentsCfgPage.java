package com.agileai.miscdp.hotweb.ui.editors.treecontent;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
/**
 * 明细Jsp配置页面
 */
public class TACContentsCfgPage extends Composite{
	private TabFolder tabFolder;
	private TreeContentFuncModel funcModel = null;
	private TreeContentModelEditor modelEditor = null;
	private TACTreeEditArea treeEditArea = null;
	private List<TACContentListArea> taContentListAreas = null;
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public TACContentsCfgPage(Composite parent,TreeContentModelEditor modelEditor, int style) {
		super(parent, style);
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();
		this.createContent();
	}
	private void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		setLayout(gridLayout);
		
		tabFolder = new TabFolder(this, SWT.NONE);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
//		toolkit.adapt(tabFolder, true, true);
		
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
//		toolkit.adapt(tabFolder, true, true);
		
		this.refreshContentConfig();
	}
	
	public void refreshContentConfig(){
		if (tabFolder.getChildren() == null || tabFolder.getChildren().length == 0){
			initContent();
		}
		else{
			TabItem[] items = tabFolder.getItems();
			for (TabItem item : items) {
				item.dispose();
			}
			initContent();
		}
	}
	
	private void initContent(){
		if (funcModel.isManageTree()){
			final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			tabItem.setText("树形表单");
			treeEditArea = new TACTreeEditArea(tabFolder,modelEditor,SWT.NONE);
			tabItem.setControl(treeEditArea);
		}
		List<ContentTableInfo> contentTableInfos = funcModel.getContentTableInfoList();
		taContentListAreas = new ArrayList<TACContentListArea>();
		for (int i=0;i < contentTableInfos.size();i++){
			final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			ContentTableInfo contentTableInfo = contentTableInfos.get(i);
			tabItem.setText(contentTableInfo.getTableName());
			TACContentListArea taContentListArea = new TACContentListArea(tabFolder,modelEditor,contentTableInfo,SWT.NONE);	
			tabItem.setControl(taContentListArea);
			taContentListAreas.add(taContentListArea);
		}
		this.initValues();
		this.registryModifier();
	}
	
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	
	private void initValues() {
		for (int i=0;i < taContentListAreas.size();i++){
			taContentListAreas.get(i).initValues();
		}
	}
	
	private void registryModifier() {
		for (int i=0;i < taContentListAreas.size();i++){
			taContentListAreas.get(i).registryModifier();
		}		
	}
	
	public TreeContentFuncModel buildConfig() {
		List<ContentTableInfo> contentTableInfos = funcModel.getContentTableInfoList();
		for (int i=0;i < taContentListAreas.size();i++){
			TACContentListArea tacContentListArea = taContentListAreas.get(i);
			ContentTableInfo contentTableInfo = contentTableInfos.get(i);
			String queryListSql = tacContentListArea.getListSqlText().getText();
			String listTitle = tacContentListArea.getListTitleText().getText();
			contentTableInfo.setQueryListSql(queryListSql);
			contentTableInfo.setTabName(listTitle);
		}
		return this.funcModel;
	}
	public TACTreeEditArea getTreeEditArea() {
		return treeEditArea;
	}
	public List<TACContentListArea> getTaContentListAreas() {
		return taContentListAreas;
	}	
}

