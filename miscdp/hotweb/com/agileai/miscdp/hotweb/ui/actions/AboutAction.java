﻿package com.agileai.miscdp.hotweb.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import com.agileai.miscdp.hotweb.ui.dialogs.AboutDialog;
/**
 * 关于Miscdp动作
 */
public class AboutAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	public AboutAction() {
	}
	public void run(IAction action) {
		AboutDialog aboutDialog = new AboutDialog(window.getShell());
		aboutDialog.open();
	}
	public void selectionChanged(IAction action, ISelection selection) {
	}

	public void dispose() {
	}

	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}
