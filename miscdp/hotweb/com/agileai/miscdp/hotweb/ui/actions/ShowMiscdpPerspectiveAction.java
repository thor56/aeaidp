package com.agileai.miscdp.hotweb.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

import com.agileai.miscdp.DeveloperConst;
/**
 * 切换至Miscdp透视图动作
 */
public class ShowMiscdpPerspectiveAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	public ShowMiscdpPerspectiveAction() {
	}
	public void run(IAction action) {
		try {
			PlatformUI.getWorkbench().showPerspective(DeveloperConst.PERSPECTIVE_ID, window);
		} catch (WorkbenchException e) {
			e.printStackTrace();
		}
	}
	public void selectionChanged(IAction action, ISelection selection) {
	}

	public void dispose() {
	}

	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}
