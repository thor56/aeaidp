﻿package com.agileai.miscdp.hotweb.domain;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.database.DBManager;
/**
 * 编码类型
 */
public class CodeType extends KeyNamePair{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List prepareInput(String projectName,String codeGroup){
		List list = new ArrayList();
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		DBManager dbManager = DBManager.getInstance(projectName);
		try {
			connection = dbManager.createConnection();
			String sql = "select TYPE_ID,TYPE_NAME,TYPE_GROUP from SYS_CODETYPE";
			if (codeGroup != null){
				sql = sql+" where TYPE_GROUP = '"+codeGroup+"' order by TYPE_ID";
			}
			else{
				sql = sql+" order by TYPE_ID";
			}
			st = connection.createStatement();
			rs = st.executeQuery(sql);
			CodeType blank = new CodeType();
			blank.put("",DeveloperConst.BLANK_STRING);
			list.add(blank);
			while(rs.next()){
				CodeType codeType = new CodeType();
				String key = (String)rs.getString("TYPE_ID");
				String value = (String)rs.getString("TYPE_NAME");
				codeType.put(key,value);
				list.add(codeType);
			}
			connection.commit();
		} catch (Exception e) {
			e.printStackTrace();
			dbManager.rollbackConnection(connection);
		}
		finally{
			dbManager.release(rs,st, connection);
		}
		return list;
	}
}
