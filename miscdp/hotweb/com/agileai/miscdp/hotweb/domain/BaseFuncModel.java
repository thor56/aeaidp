﻿package com.agileai.miscdp.hotweb.domain;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.agileai.common.IniReader;
import com.agileai.hotweb.model.FormObjectDocument.FormObject;
import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;
/**
 * 功能模型
 */
abstract public class BaseFuncModel implements IEditorInput{
	protected String serviceId = "";
	protected String interfaceName = "";
	protected String implClassName = "";
	
	protected String listSql = "";
	protected String listJspName = "";
	
	protected String mainPkg = "";
	protected String funcName = "";
	protected String funcType = "";
	protected String funcTypeName = "";
	protected String funcSubPkg = "";
	protected String projectName = "";
	
	protected String funcId = "";
	protected String template = "Default";
	protected ResFileValueProvider resFileValueProvider = null;
	
	protected String editorId = DeveloperConst.CUSTOM_EDITOR_ID;
	
	public boolean exists() {
		return false;
	}
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	public IPersistableElement getPersistable() {
		return null;
	}
	public static String getTipText(String funcId,String funcName){
		return "功能标识："+funcId+"  "+"功能名称："+funcName;
	}
	public String getToolTipText() {
		return getTipText(funcId, funcName);
	}
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class adapter) {
		return null;
	}
	public String getName() {
		return this.funcName;
	}
	public String getFuncName() {
		return funcName;
	}
	public void setFuncName(String funcName){
		this.funcName = funcName;
	}
	
	public String getFuncSubPkg() {
		return funcSubPkg == null?"":funcSubPkg;
	}
	public void setFuncSubPkg(String funcSubPkg) {
		this.funcSubPkg = funcSubPkg;
	}
	public String getFuncType() {
		return funcType;
	}
	public void setFuncType(String funcType) {
		this.funcType = funcType;
	}
	public String getFuncId() {
		return funcId;
	}
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}
	public String getEditorId() {
		return editorId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	public String getImplClassName() {
		return implClassName;
	}
	public void setImplClassName(String implClassName) {
		this.implClassName = implClassName;
	}
	public String getListSql() {
		return listSql;
	}
	public void setListSql(String listSql) {
		if (listSql != null)
		this.listSql = listSql;
	}
	public String getListJspName() {
		return listJspName;
	}
	public void setListJspName(String listJspName) {
		this.listJspName = listJspName;
	}
	abstract public void buildFuncModel(String funcDefine);
	
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getFuncTypeName() {
		return funcTypeName;
	}
	public void setFuncTypeName(String funcTypeName) {
		this.funcTypeName = funcTypeName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getMainPkg() {
		return mainPkg;
	}
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	public void processResFileValueProvider(FormObject[] editFormObjects){
		for (int i=0;i < editFormObjects.length;i++){
			com.agileai.hotweb.model.FormObjectDocument.FormObject formObject = editFormObjects[i];
			if (formObject.isSetResFile()){
				com.agileai.hotweb.model.ResFileType resFile = formObject.getResFile();
				String valueProvider = resFile.getValueProvider();
				if (!StringUtil.isNullOrEmpty(valueProvider)){
					try {
						JSONObject jsonObject = new JSONObject(valueProvider);
						String tableName = jsonObject.getString(ResFileValueProvider.TableNameKey);
						String primaryKeyField = jsonObject.getString(ResFileValueProvider.PrimaryKeyFieldKey);
						String bizIdField = jsonObject.getString(ResFileValueProvider.BizIdFieldKey);
						String resIdField = jsonObject.getString(ResFileValueProvider.ResIdFieldKey);
						
						this.resFileValueProvider = new ResFileValueProvider();
						resFileValueProvider.setTableName(tableName);
						resFileValueProvider.setPrimaryKeyField(primaryKeyField);
						resFileValueProvider.setBizIdField(bizIdField);
						resFileValueProvider.setResIdField(resIdField);
					} catch (Exception e) {
						MiscdpPlugin.getDefault().logError(e.getLocalizedMessage(), e);
					}
				}
				break;
			}
		}
	}
	
	public static void processListTableColumns(com.agileai.hotweb.model.ListTableType.Row.Column[] columns,List<ListTabColumn> tableColumns){
		boolean onOpen = false;
		if (columns.length > 0 && tableColumns.size() == 0){
			onOpen = true;
		}
		for (int i=0;i < columns.length;i++){
			com.agileai.hotweb.model.ListTableType.Row.Column column = columns[i];
			ListTabColumn listTabColumn = null;
			if (onOpen){
				listTabColumn = new ListTabColumn();
				tableColumns.add(listTabColumn);
			}else{
				listTabColumn = tableColumns.get(i);				
			}
			listTabColumn.setProperty(column.getProperty());
			listTabColumn.setTitle(column.getTitle());
			listTabColumn.setWidth(column.getWidth());
			listTabColumn.setCell(column.getCell());
			listTabColumn.setFormat(column.getFormat());
			listTabColumn.setMappingItem(column.getMappingItem());
		}
	}
	public static void processPageFormFields(FormObject[] editFormObjects,List<PageFormField> pageFormFields){
		boolean onOpen = false;
		if (editFormObjects.length > 0 && pageFormFields.size() == 0){
			onOpen = true;
		}
		for (int i=0;i < editFormObjects.length;i++){
			com.agileai.hotweb.model.FormObjectDocument.FormObject formObject = editFormObjects[i];
			PageFormField pageFormField = null;
			if (onOpen){
				pageFormField = new PageFormField();
				pageFormFields.add(pageFormField);
			}else{
				pageFormField = pageFormFields.get(i);				
			}
			pageFormField.setLabel(formObject.getLabel());
			pageFormField.setFormat(formObject.getFormat().toString());
			pageFormField.setDataType(formObject.getDataType().toString());
			
			if (formObject.isSetText()){
				com.agileai.hotweb.model.TextType text = formObject.getText();
				pageFormField.setCode(text.getId());
				pageFormField.setTagType("text");	
				pageFormField.setDefaultValue(text.getDefValue().getValue(),text.getDefValue().getIsVariable());
				pageFormField.setDataLength(text.getSize());
			}
			else if (formObject.isSetHidden()){
				com.agileai.hotweb.model.HiddenType hidden = formObject.getHidden();
				pageFormField.setCode(hidden.getId());
				pageFormField.setTagType("hidden");	
				if (hidden.getIsPK() != null){
					pageFormField.setIsPK(hidden.getIsPK());
				}
				pageFormField.setDefaultValue(hidden.getDefValue().getValue(),hidden.getDefValue().getIsVariable());
			}
			else if (formObject.isSetSelect()){
				com.agileai.hotweb.model.SelectType select = formObject.getSelect();
				pageFormField.setCode(select.getId());
				pageFormField.setTagType("select");
				pageFormField.setDefaultValue(select.getDefValue().getValue(),select.getDefValue().getIsVariable());
				pageFormField.setValueProvider(select.getValueProvider());
			}
			else if (formObject.isSetRadio()){
				com.agileai.hotweb.model.RadioType radio = formObject.getRadio();
				pageFormField.setCode(radio.getId());
				pageFormField.setTagType("radio");
				pageFormField.setDefaultValue(radio.getDefValue().getValue(),radio.getDefValue().getIsVariable());
				pageFormField.setValueProvider(radio.getValueProvider());
			}			
			else if (formObject.isSetCheckBox()){
				com.agileai.hotweb.model.CheckBoxType checkBox = formObject.getCheckBox();
				pageFormField.setCode(checkBox.getId());
				pageFormField.setTagType("checkbox");
				pageFormField.setDefaultValue(checkBox.getDefValue().getValue(),checkBox.getDefValue().getIsVariable());
				pageFormField.setValueProvider(checkBox.getValueProvider());
			}	
			else if (formObject.isSetPopup()){
				com.agileai.hotweb.model.PopupType popup = formObject.getPopup();
				pageFormField.setCode(popup.getId());
				pageFormField.setTagType("popup");
				pageFormField.setDefaultValue(popup.getDefValue().getValue(),popup.getDefValue().getIsVariable());
				pageFormField.setValueProvider(popup.getValueProvider());
			}			
			else if (formObject.isSetResFile()){
				com.agileai.hotweb.model.ResFileType resFile = formObject.getResFile();
				pageFormField.setCode(resFile.getId());
				pageFormField.setTagType("resfile");
				pageFormField.setDefaultValue(resFile.getDefValue().getValue(),resFile.getDefValue().getIsVariable());
				pageFormField.setValueProvider(resFile.getValueProvider());
			}
			else if (formObject.isSetTextArea()){
				com.agileai.hotweb.model.TextAreaType textArea = formObject.getTextArea();
				pageFormField.setCode(textArea.getId());
				pageFormField.setTagType("textarea");
				pageFormField.setDefaultValue(textArea.getDefValue().getValue(),textArea.getDefValue().getIsVariable());
			}
			else if (formObject.isSetContent()){
				com.agileai.hotweb.model.LabelType content = formObject.getContent();
				pageFormField.setCode(content.getId());
				pageFormField.setTagType("label");	
				pageFormField.setDefaultValue(content.getDefValue().getValue(),content.getDefValue().getIsVariable());
			}
			com.agileai.hotweb.model.ValidateType validateType[] = formObject.getValidationArray();
			if (validateType != null){
				for (int j = 0;j < validateType.length;j++){
					Validation validation = null;
					if (onOpen){
						validation = new Validation();
						pageFormField.getValidations().add(validation);
					}else{
						validation = pageFormField.getValidations().get(j);
					}
					String key = validateType[j].getType().toString();
					String text = MiscdpUtil.getValidationText(key);
					validation.put(key, text);
					validation.setParam0(validateType[j].getParam0());
					validation.setParam1(validateType[j].getParam1());
										
				}
			}
		}
	}
	
	public static void processPageParamters(FormObject[] formObjects,List<PageParameter> pageParameters){
		boolean onOpen = false;
		if (formObjects.length > 0 && pageParameters.size() == 0){
			onOpen = true;
		}
		for (int i=0;i < formObjects.length;i++){
			FormObject formObject = formObjects[i];
			PageParameter pageParameter = null;
			if (onOpen){
				pageParameter = new PageParameter();
				pageParameters.add(pageParameter);
			}else{
				pageParameter = pageParameters.get(i);				
			}
			
			pageParameter.setLabel(formObject.getLabel());
			if (formObject.getFormat() != null){
				pageParameter.setFormat(formObject.getFormat().toString());					
			}
			if (formObject.getDataType() != null){
				pageParameter.setDataType(formObject.getDataType().toString());					
			}
			
			if (formObject.isSetText()){
				com.agileai.hotweb.model.TextType text = formObject.getText();
				pageParameter.setCode(text.getId());
				pageParameter.setTagType("text");	
				pageParameter.setDefaultValue(text.getDefValue().getValue(),text.getDefValue().getIsVariable());
			}
			else if (formObject.isSetHidden()){
				com.agileai.hotweb.model.HiddenType hidden = formObject.getHidden();
				pageParameter.setCode(hidden.getId());
				pageParameter.setTagType("hidden");	
				pageParameter.setDefaultValue(hidden.getDefValue().getValue(),hidden.getDefValue().getIsVariable());
			}
			else if (formObject.isSetSelect()){
				com.agileai.hotweb.model.SelectType select = formObject.getSelect();
				pageParameter.setCode(select.getId());
				pageParameter.setTagType("select");
				pageParameter.setDefaultValue(select.getDefValue().getValue(),select.getDefValue().getIsVariable());
				pageParameter.setValueProvider(select.getValueProvider());
			}
			else if (formObject.isSetRadio()){
				com.agileai.hotweb.model.RadioType radio = formObject.getRadio();
				pageParameter.setCode(radio.getId());
				pageParameter.setTagType("radio");
				pageParameter.setDefaultValue(radio.getDefValue().getValue(),radio.getDefValue().getIsVariable());
				pageParameter.setValueProvider(radio.getValueProvider());
			}
			else if (formObject.isSetCheckBox()){
				com.agileai.hotweb.model.CheckBoxType checkBox = formObject.getCheckBox();
				pageParameter.setCode(checkBox.getId());
				pageParameter.setTagType("checkbox");
				pageParameter.setDefaultValue(checkBox.getDefValue().getValue(),checkBox.getDefValue().getIsVariable());
				pageParameter.setValueProvider(checkBox.getValueProvider());
			}		
			else if (formObject.isSetPopup()){
				com.agileai.hotweb.model.PopupType popup = formObject.getPopup();
				pageParameter.setCode(popup.getId());
				pageParameter.setTagType("popup");
				pageParameter.setDefaultValue(popup.getDefValue().getValue(),popup.getDefValue().getIsVariable());
				pageParameter.setValueProvider(popup.getValueProvider());
			}
			com.agileai.hotweb.model.ValidateType validateType[] = formObject.getValidationArray();
			if (validateType != null){
				for (int j = 0;j < validateType.length;j++){
					Validation validation = new Validation();
					if (onOpen){
						validation = new Validation();
						pageParameter.getValidations().add(validation);
					}else{
						validation = pageParameter.getValidations().get(j);
					}
					String key = validateType[j].getType().toString();
					String text = MiscdpUtil.getValidationText(key);
					validation.put(key, text);
					validation.setParam0(validateType[j].getParam0());
					validation.setParam1(validateType[j].getParam1());
				}
			}
		}
	}	
	
	public static void initPageFormFields(String[] columns,List<PageFormField> pageFormFieldList){
		if (pageFormFieldList.size() > 0){
			pageFormFieldList.clear();
		}
		for (int i=0;i < columns.length;i++){
			PageFormField pageFormField = new PageFormField();
			pageFormField.setLabel(columns[i]);
			pageFormField.setCode(columns[i]);
			pageFormField.setDataType("string");
			pageFormField.setTagType("text");
			pageFormFieldList.add(pageFormField);
		}
	}
	
	public static void initPageFormFields(Column[] columns,List<PageFormField> pageFormFieldList,List<String> hiddeFields){
		if (pageFormFieldList.size() > 0){
			pageFormFieldList.clear();
		}
		IniReader reader = MiscdpUtil.getIniReader();
		for (int i=0;i < columns.length;i++){
			PageFormField pageFormField = new PageFormField();
			pageFormField.setLabel(columns[i].getLabel());
			pageFormField.setCode(columns[i].getName());
			String columnClassName = columns[i].getClassName(); 
			if (columns[i].isPK()&& ((columnClassName.equals("java.lang.Integer") || columnClassName.equals("java.lang.Long"))
					|| (columnClassName.equals("java.lang.String") && columns[i].getLength() == 36))){
				pageFormField.setTagType("hidden");
				pageFormField.setIsPK("Y");
			}else{
				if (hiddeFields!= null && hiddeFields.contains(columns[i].getName())){
					pageFormField.setTagType("hidden");
				}else{
					pageFormField.setTagType("text");					
				}
			}

			pageFormField.setDataType(MiscdpUtil.getDataType(columns[i].getClassName()));
			pageFormField.setDataLength(columns[i].getLength());
			
			if (!columns[i].isNullable()){
				Validation validation = new Validation();
				String key = "must_not_empty";
				String text = reader.getValue("Validation",key);
				validation.put(key,text);
				if (!pageFormField.getTagType().equals("hidden")){
					pageFormField.addValidation(validation);					
				}
			}
			if ("java.sql.Timestamp".equals(columns[i].getClassName())){
				pageFormField.setFormat("yyyy-MM-dd HH:mm");
				Validation validation = new Validation();
				String key = "must_be_date";
				String text = reader.getValue("Validation",key);
				validation.put(key,text);
				if (!pageFormField.getTagType().equals("hidden")){
					pageFormField.addValidation(validation);					
				}
			}
			else if ("java.sql.Date".equals(columns[i].getClassName())){
				pageFormField.setFormat("yyyy-MM-dd");
				Validation validation = new Validation();
				String key = "must_be_date";
				String text = reader.getValue("Validation",key);
				validation.put(key,text);
				if (!pageFormField.getTagType().equals("hidden")){
					pageFormField.addValidation(validation);					
				}
			}
			if ("java.lang.Integer".equals(columns[i].getClassName())
					|| "java.lang.Long".equals(columns[i].getClassName())){
				Validation validation = new Validation();
				String key = "must_be_int";
				String text = reader.getValue("Validation",key);
				validation.put(key,text);
				if (!pageFormField.getTagType().equals("hidden")){
					pageFormField.addValidation(validation);					
				}
			}
			else if ("numeric".equals(pageFormField.getDataType())){
				Validation validation = new Validation();
				String key = "must_be_num";
				String text = reader.getValue("Validation",key);
				validation.put(key,text);
				if (!pageFormField.getTagType().equals("hidden")){
					pageFormField.addValidation(validation);					
				}
			}
//			if ("java.lang.String".equals(columns[i].getClassName())){
//				Validation validation = new Validation();
//				String key = "length_limit";
//				String text = reader.getValue("Validation",key);
//				validation.put(key,text);
//				validation.setParam0(String.valueOf(columns[i].getLength()));
//				pageFormField.addValidation(validation);
//			}
			pageFormFieldList.add(pageFormField);
		}
	}
	
	public static void initPageParameters(List<String> params,List<PageParameter> pageParameterList){
		if (pageParameterList.size() > 0){
			pageParameterList.clear();
		}
		for (int i=0;i < params.size();i++){
			String param = (String)params.get(i);
			PageParameter pageParameter = new PageParameter();
			pageParameter.setLabel(param);
			pageParameter.setCode(param);
			pageParameter.setDataType("string");
			pageParameter.setTagType("text");	
			pageParameterList.add(pageParameter);
		}
	}
	public static void initListTableColumns(String[] columns,List<ListTabColumn> tableColumnList){
		if (tableColumnList != null){
			tableColumnList.clear();
		}
		for (int i=0;i < columns.length;i++){
			ListTabColumn listTabColumn = new ListTabColumn();
			listTabColumn.setProperty(columns[i]);
			listTabColumn.setTitle(columns[i]);
			listTabColumn.setWidth("100");
			listTabColumn.setCell("string");
			tableColumnList.add(listTabColumn);				
		}
	}
	
	public static void initListTableColumns(Column[] columns,List<ListTabColumn> tableColumnList,List<String> rsIdColumnList){
		if (tableColumnList.size() > 0){
			tableColumnList.clear();
		}
		if (rsIdColumnList != null && rsIdColumnList.size() > 0){
			rsIdColumnList.clear();
		}
		for (int i=0;i < columns.length;i++){
			if (rsIdColumnList != null && columns[i].isPK()){
				rsIdColumnList.add(columns[i].getName());
				continue;
			}
			ListTabColumn listTabColumn = new ListTabColumn();
			listTabColumn.setProperty(columns[i].getName());
			listTabColumn.setTitle(columns[i].getLabel());
			listTabColumn.setWidth("100");
			listTabColumn.setCell(MiscdpUtil.getDataType(columns[i].getClassName()));
			if ("java.sql.Timestamp".equals(columns[i].getClassName())){
				listTabColumn.setFormat("yyyy-MM-dd HH:mm");				
			}
			else if ("java.sql.Date".equals(columns[i].getClassName())){
				listTabColumn.setFormat("yyyy-MM-dd");
			}
			tableColumnList.add(listTabColumn);				
		}
	}
	
	public String retrieveUniqueField(List<PageFormField> pageFormFields){
		String result = null;
		for (int i=0;i < pageFormFields.size();i++){
			PageFormField pageFormField = pageFormFields.get(i);
			for (int j=0;j < pageFormField.getValidations().size();j++){
				Validation validation = pageFormField.getValidations().get(j);
				if (Validation.Values.MUST_BE_UNIQUE.equals(validation.getKey())){
					result = pageFormField.getCode();
					break;
				}
			}
		}
		return result;
	}
	
	public ResFileValueProvider getResFileValueProvider() {
		return resFileValueProvider;
	}
}
