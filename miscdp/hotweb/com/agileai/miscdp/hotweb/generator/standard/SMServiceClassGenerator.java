﻿package com.agileai.miscdp.hotweb.generator.standard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.ResFileValueProvider;
import com.agileai.miscdp.hotweb.domain.standard.StandardFuncModel;
import com.agileai.miscdp.util.MiscdpUtil;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
/**
 * 服务实现类代码生成器
 */
public class SMServiceClassGenerator {
	private String charencoding = "UTF-8";
	private String templateDir = null;
	private String javaFile = null;
	private File xmlFile = null;
	private StandardFuncModel funcModel = null;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
        	Configuration cfg = new Configuration();
        	cfg.setEncoding(Locale.getDefault(), charencoding);
        	cfg.setDirectoryForTemplateLoading(new File(templateDir));
            cfg.setObjectWrapper(new DefaultObjectWrapper());
        	String templateFile = "/standard/ServiceImpl.java.ftl";
        	Template temp = cfg.getTemplate(templateFile,charencoding);
        	temp.setEncoding(charencoding);
            Map root = new HashMap();
            NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
            root.put("doc",nodeModel);
            if (funcModel.getResFileValueProvider() != null){
                ResFileValueProvider resFileValueProvider = funcModel.getResFileValueProvider();
                String refTableName = resFileValueProvider.getTableName();
                String sqlNameSpace = MiscdpUtil.getValidName(refTableName);
            	HashMap resFileModel = new HashMap();
            	resFileModel.put("bizIdField", resFileValueProvider.getBizIdField());
            	resFileModel.put("sqlNameSpace", sqlNameSpace);
            	root.put("resFileModel", resFileModel);
            }else{
            	root.put("resFileModel", "");
            }
            File file = new File(javaFile);
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charencoding));
//            Writer out = new OutputStreamWriter(System.out);
            temp.process(root, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}
	public void setJavaFile(String javaFile) {
		this.javaFile = javaFile;
	}
	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}
	public String getCharencoding() {
		return charencoding;
	}
	public void setFuncModel(StandardFuncModel funcModel) {
		this.funcModel = funcModel;
	}
}
