
package com.agileai.miscdp.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createContext complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createContext">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="integrateApp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createContext", propOrder = {
    "appName",
    "docBase",
    "integrateApp"
})
public class CreateContext {

    protected String appName;
    protected String docBase;
    protected boolean integrateApp;

    /**
     * Gets the value of the appName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Sets the value of the appName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppName(String value) {
        this.appName = value;
    }

    /**
     * Gets the value of the docBase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocBase() {
        return docBase;
    }

    /**
     * Sets the value of the docBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocBase(String value) {
        this.docBase = value;
    }

    /**
     * Gets the value of the integrateApp property.
     * 
     */
    public boolean isIntegrateApp() {
        return integrateApp;
    }

    /**
     * Sets the value of the integrateApp property.
     * 
     */
    public void setIntegrateApp(boolean value) {
        this.integrateApp = value;
    }

}
