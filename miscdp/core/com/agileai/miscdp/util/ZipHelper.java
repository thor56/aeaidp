package com.agileai.miscdp.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

public class ZipHelper {
	private ZipFile zipFile; 
    private ZipOutputStream zipOut; 
    private int bufSize; 
    private byte[] buf; 
    private int readedBytes; 
    private String zipDirectory = null;
    
    public ZipHelper(){ 
        this(512); 
    } 

    public ZipHelper(int bufSize){ 
        this.bufSize = bufSize; 
        this.buf = new byte[this.bufSize]; 
    } 
    
    public void doZip(String dirOrFile){ 
    	this.zipDirectory = dirOrFile;
        File zipDir = new File(dirOrFile); 
        String zipFileName = zipDir.getPath() + ".zip"; 
        try{ 
            this.zipOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileName)));
            handleDir(zipDir , this.zipOut); 
            this.zipOut.close(); 
        }catch(IOException ioe){ 
        	ioe.printStackTrace();
        } 
    } 
    public void doZip(String dirOrFile,String targetFullZipName){ 
    	this.zipDirectory = dirOrFile;
        File zipDir = new File(dirOrFile); 
        try{ 
            this.zipOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(targetFullZipName)));
            handleDir(zipDir , this.zipOut); 
            this.zipOut.close(); 
        }catch(IOException ioe){ 
        	ioe.printStackTrace();
        } 
    }
    
    private void handleDir(File dirOrFile,ZipOutputStream zipOut)throws IOException{
        FileInputStream fileIn; 
        if (dirOrFile.isFile()){
            fileIn = new FileInputStream(dirOrFile); 
            String fileName = dirOrFile.getName();
            this.zipOut.putNextEntry(new ZipEntry(fileName));
            while((this.readedBytes = fileIn.read(this.buf))>0){ 
                this.zipOut.write(this.buf ,0,this.readedBytes); 
            } 
            this.zipOut.closeEntry(); 
        }else{
            File[] files = dirOrFile.listFiles(); 
            if(files.length == 0){ 
                this.zipOut.putNextEntry(new ZipEntry(dirOrFile.toString() + "/")); 
                this.zipOut.closeEntry(); 
            } 
            else{ 
                for(File file : files){ 
                    if(file.isDirectory()){ 
                        handleDir(file , this.zipOut); 
                    } 
                    else{ 
                        fileIn = new FileInputStream(file); 
                        String fileName = file.getPath().substring(this.zipDirectory.length()+1);
                        this.zipOut.putNextEntry(new ZipEntry(fileName));
                        while((this.readedBytes = fileIn.read(this.buf))>0){ 
                            this.zipOut.write(this.buf ,0,this.readedBytes); 
                        } 
                        this.zipOut.closeEntry(); 
                    } 
                } 
            }         	
        }
    } 

    @SuppressWarnings("rawtypes")
	public void unZip(String zipfileName,String destinationDir){
    	destinationDir = destinationDir.replaceAll("\\\\", "/");
    	if (!destinationDir.endsWith("/")){
    		destinationDir = destinationDir + "/";
    	}
        FileOutputStream fileOut; 
        InputStream inputStream; 
        try{ 
        	File pathFile = new File(destinationDir); 
            if(!pathFile.exists()){ 
                 pathFile.mkdirs(); 
            } 
            
            this.zipFile = new ZipFile(zipfileName); 
            Enumeration entries = this.zipFile.getEntries();
            while(entries.hasMoreElements()){
                ZipEntry entry = (ZipEntry)entries.nextElement(); 
                String zipEntryName = entry.getName(); 
                inputStream = zipFile.getInputStream(entry); 
                String outPath = (destinationDir+zipEntryName).replaceAll("\\\\", "/");
                
                if (entry.isDirectory()){
                	continue;
                }
                
                File outFile = new File(outPath);
                File parentFile = outFile.getParentFile();
                if (!parentFile.exists()){
                	parentFile.mkdirs();
                }
                if (!outFile.exists()){
                	outFile.createNewFile();
                }
                fileOut = new FileOutputStream(outPath); 
                while(( this.readedBytes = inputStream.read(this.buf) ) > 0){
                     fileOut.write(this.buf ,0,this.readedBytes ); 
                } 
                fileOut.close(); 
                inputStream.close(); 
            }
            
            this.zipFile.close(); 
        }catch(IOException ioe){ 
        	ioe.printStackTrace();
        } 
    }
}
