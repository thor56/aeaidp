<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treefill",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeFillFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
package ${Util.parsePkg(handler.@selectTreeHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@selectTreeHandlerClass)} extends TreeSelectHandler{
	public ${Util.parseClass(handler.@selectTreeHandlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.isMuilSelect = ${baseInfo.@isMultiSelect};
	}
	protected TreeBuilder provideTreeBuilder(DataParam param){
		List<DataRow> records = getService().queryPickTreeRecords(param);
		TreeBuilder treeBuilder = new TreeBuilder(records,"${baseInfo.@nodeIdField}","${baseInfo.@nodeNameField}","${baseInfo.@nodePIdField}");
		<#if Util.isValidValue(baseInfo.@nodeTypeField)>
		treeBuilder.setTypeKey("${baseInfo.@nodeTypeField}");
		</#if>
		<#if Util.isValidValue(baseInfo.@rootIdParamKey)>
		String rootId = param.get("${baseInfo.@rootIdParamKey}");
		treeBuilder.setRootId(rootId);
		</#if>
		<#if Util.isValidValue(baseInfo.@excludeIdParamKey)>
		String excludeId = param.get("${baseInfo.@excludeIdParamKey}");
		treeBuilder.getExcludeIds().add(excludeId);
		</#if>
		return treeBuilder;
	}

	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}	
</#macro>	
