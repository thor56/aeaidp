<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">
  <select id="queryPickFillRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.findRecordsSql}
  </select>
</sqlMap>