<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@InterfaceName)};

import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface ${Util.parseClass(service.@InterfaceName)} extends StandardService{

}
</#macro>