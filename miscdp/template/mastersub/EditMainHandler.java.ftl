<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/mastersub",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro MasterSubFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local masterArea = .node.EditMainView.MasterEditArea>
package ${Util.parsePkg(handler.@editHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.domain.*;
import com.agileai.hotweb.domain.*;
import com.agileai.util.*;

public class ${Util.parseClass(handler.@editHandlerClass)} extends MasterSubEditMainHandler{
	public ${Util.parseClass(handler.@editHandlerClass)}(){
		super();
		this.listHandlerClass = ${Util.parseClass(handler.@listHandlerClass)}.class;
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.baseTablePK = "${baseInfo.@tablePK}";
		this.defaultTabId = "${baseInfo.@defaultTabId}";
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(masterArea)><#list masterArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/>
		</#list></#if>
		<#list .node.EditMainView.SubEntryEditArea as entryEditArea>
			<#list entryEditArea["fa:FormObject"] as formObject><@SetRowAttribute formAtom=formObject/>
			</#list>			
		</#list>
		<#list .node.EditMainView.SubListViewArea as subListViewArea>
		<#local row =subListViewArea["fa:Row"][0]>
		<#list row["fa:Column"] as columnVar><@InitMappingItem column=columnVar></@InitMappingItem></#list>				
		</#list>
		</#compress>
		
	}
	protected String[] getEntryEditFields(String currentSubTableId){
		List<String> temp = new ArrayList<String>();
		<#list .node.EditMainView.SubEntryEditArea as entryEditArea>
		if ("${entryEditArea.@subTableId}".equals(currentSubTableId)){
			<#list entryEditArea["fa:FormObject"] as formAtom><#local atom = formAtom.*[0]>temp.add("${atom["fa:Name"]}");
			</#list>			
		}
		</#list>
		return temp.toArray(new String[]{});
	}
	protected String getEntryEditTablePK(String currentSubTableId){
		HashMap<String,String> primaryKeys = new HashMap<String,String>();
		<#list .node.BaseInfo.SubTableInfo as subTableInfo>
		primaryKeys.put("${subTableInfo.@subTableId}","${subTableInfo.@primaryKey}");
		</#list>
		return primaryKeys.get(currentSubTableId);
	}
	protected String getEntryEditForeignKey(String currentSubTableId){
		HashMap<String,String> foreignKeys = new HashMap<String,String>();
		<#list .node.BaseInfo.SubTableInfo as subTableInfo>
		foreignKeys.put("${subTableInfo.@subTableId}","${subTableInfo.@foreignKey}");
		</#list>
		return foreignKeys.get(currentSubTableId);	
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>

<#macro SetRowAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if type="select">
	setAttribute("${atom["fa:Name"]}Select",FormSelectFactory.create("${atom["fa:ValueProvider"]}"));</#if>
</#macro>

<#macro InitMappingItem column>
	<#if Util.isValidValue(column.@mappingItem)>
	initMappingItem("${column.@property}",FormSelectFactory.create("${column.@mappingItem}").getContent());
	</#if>
</#macro>