package com.agileai.hotweb.domain.system;

import java.io.Serializable;

public class Operation implements Serializable{
	private static final long serialVersionUID = -7804182828070948988L;
	
	public static final String CTLTYPE_DISABLE = "disableMode";
	public static final String CTLTYPE_HIDDEN = "hiddenMode";
	private String operId;
	private String operName;
	private String operCode;
	private String handlerId;
	private String actionType;

	public String getOperCode() {
		return operCode;
	}
	public void setOperCode(String operCode) {
		this.operCode = operCode;
	}
	public String getOperId() {
		return operId;
	}
	public void setOperId(String operId) {
		this.operId = operId;
	}
	public String getOperName() {
		return operName;
	}
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
}