package com.agileai.hotweb.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.hotweb.cxmodule.SecurityRoleTreeSelect;

public class SecurityRoleTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityRoleTreeSelect {
    public SecurityRoleTreeSelectImpl() {
        super();
    }
}
