package com.agileai.hotweb.config;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.agileai.common.AppConfig;

public class AppConfigFactoryBean implements FactoryBean{
	private String configFile = "ConfigContext.xml";
	
	@Override
	public Object getObject() throws Exception {
		Object result = null;
		Resource res = new ClassPathResource(configFile);
		BeanFactory factory = new XmlBeanFactory(res);
		result = factory.getBean("appConfig");
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class getObjectType() {
		return AppConfig.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public String getConfigFile() {
		return configFile;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}
}
