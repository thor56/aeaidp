package com.agileai.hotweb.renders;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectRenderer extends ViewRenderer {
	private String redirectUrl = null; 
	public RedirectRenderer(String redirectUrl){
		this.redirectUrl = redirectUrl;
	}
	public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws IOException{
		response.sendRedirect(redirectUrl);
	}
}
