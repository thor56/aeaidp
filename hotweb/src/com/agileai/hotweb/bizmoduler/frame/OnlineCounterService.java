package com.agileai.hotweb.bizmoduler.frame;

public interface OnlineCounterService {

	public abstract int queryOnlineCount();

	public abstract void addOnlineCount();

	public abstract void minusOnlineCount();
	
	public abstract void initOnlineCount();

}