package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface ResourceUploadService extends BaseInterface{
	public void createResourceRelation(String bizId,String resId);
	public void deleteResourceRelations(List<String> refIdList);
	public List<DataRow> findResourceRelations(String bizId);
	public List<DataRow> findResourceRelations(DataParam param);
}
