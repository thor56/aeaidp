package com.agileai.hotweb.bizmoduler.core;

import java.util.Date;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;

public class SystemLogServiceImpl extends BaseService implements SystemLogService{
	public void insertLogRecord(String ipAddress,String userId,String userName
			,String funcName,String actionType){
		DataParam param = new DataParam();
		KeyGenerator keyGenerator = new KeyGenerator();
		param.put("ID",keyGenerator.genKey());
		param.put("OPER_TIME",new Date());
		param.put("IP_ADDTRESS",ipAddress);
		param.put("USER_ID",userId);
		param.put("USER_NAME",userName);
		param.put("FUNC_NAME",funcName);
		param.put("ACTION_TYPE",actionType);
		this.daoHelper.insertRecord("syslog.insertRecord", param);
	}
}
