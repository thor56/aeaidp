package com.agileai.hotweb.bizmoduler.core;

import java.util.Iterator;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public abstract class TreeManageImpl extends BaseService implements TreeManage{
	protected String idField = "MENU_ID";
	protected String nameField = "MENU_NAME";
	protected String parentIdField = "MENU_PID";
	protected String sortField = "MENU_SORT";	
	
	public List<DataRow> findTreeRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryTreeRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}
	public List<DataRow> queryPickTreeRecords(DataParam param) {
		return this.findTreeRecords(param);
	}	
	public List<DataRow> queryChildRecords(String currentId) {
		String statementId = this.sqlNameSpace+"."+"queryChildRecords";
		return this.daoHelper.queryRecords(statementId, currentId);
	}
	public DataRow queryCurrentRecord(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"queryTreeRecord";
		return this.daoHelper.getRecord(statementId, param);
	}
	public void deleteCurrentRecord(String currentId) {
		String statementId = this.sqlNameSpace+"."+"deleteTreeRecord";
		this.daoHelper.deleteRecords(statementId, currentId);
	}
	protected int getNewMaxSort(String parentId){
		int result = 0;
		String statementId = this.sqlNameSpace+"."+"queryMaxSortId";
		DataRow row = this.daoHelper.getRecord(statementId, parentId);
		String maxMenuSort = row.stringValue("MAX_"+sortField);
		if (!StringUtil.isNullOrEmpty(maxMenuSort)){
			result = Integer.parseInt(maxMenuSort)+1;
		}else{
			result =1;
		}
		return result;
	}
	public void changeCurrentSort(String currentId, boolean isUp) {
		String statementId = this.sqlNameSpace+"."+"queryCurLevelRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, currentId);
		DataRow curRow = null;
		String curSort = null;
		if (isUp){
			DataRow beforeRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(idField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					beforeRow = records.get(i-1);
					break;
				}
			}
			curSort = curRow.stringValue(sortField);
			String beforeSort = beforeRow.stringValue(sortField);;
			curRow.put(sortField,beforeSort);
			beforeRow.put(sortField,curSort);
			this.updateCurrentRecord(curRow.toDataParam());
			this.updateCurrentRecord(beforeRow.toDataParam());
		}else{
			DataRow nextRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(idField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					nextRow = records.get(i+1);
					break;
				}
			}
			curSort = curRow.stringValue(sortField);
			String nextSort = nextRow.stringValue(sortField);
			curRow.put(sortField,nextSort);
			nextRow.put(sortField,curSort);
			this.updateCurrentRecord(curRow.toDataParam());
			this.updateCurrentRecord(nextRow.toDataParam());
		}
	}
	public boolean isFirstChild(DataParam param) {
		boolean result = false;
		String statementId = this.sqlNameSpace+"."+"queryCurLevelRecords";
		String currentId = param.get(idField);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, currentId);
		if (!ListUtil.isNullOrEmpty(records)){
			DataRow firstRow = records.get(0);
			String tempMenuId = firstRow.stringValue(idField);
			if (currentId.equals(tempMenuId)){
				result = true;
			}
		}
		return result;
	}
	public boolean isLastChild(DataParam param) {
		boolean result = false;
		String statementId = this.sqlNameSpace+"."+"queryCurLevelRecords";
		String currentId = param.get(idField);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, currentId);
		if (!ListUtil.isNullOrEmpty(records)){
			DataRow firstRow = records.get(records.size()-1);
			String tempMenuId = firstRow.stringValue(idField);
			if (currentId.equals(tempMenuId)){
				result = true;
			}
		}
		return result;
	}
	public void insertChildRecord(DataParam param) {
		String parentId = param.get(idField);
		String newMenuSort = String.valueOf(this.getNewMaxSort(parentId));
		param.put("CHILD_"+sortField,newMenuSort);
		param.put("CHILD_"+idField,KeyGenerator.instance().genKey());
		param.put("CHILD_"+parentIdField,param.get(idField));
		String statementId = this.sqlNameSpace+"."+"insertTreeRecord";
		this.daoHelper.insertRecord(statementId, param);
	}	
	public void copyCurrentRecord(String currentId) {
		DataParam queryParam = new DataParam(idField,currentId);
		DataRow row = this.queryCurrentRecord(queryParam);
		DataParam param = row.toDataParam();
		param.put(idField,param.get(parentIdField));
		DataParam newParam = this.copyChildField(param);
		newParam.put("CHILD_"+nameField,param.get(nameField)+"_copy");
		this.insertChildRecord(newParam);
	}
	protected DataParam copyChildField(DataParam param){
		DataParam result = new DataParam();
		result.append(param);
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			String newKey = "CHILD_"+key;
			result.put(newKey,param.get(key));
		}
		return result;
	}
	public void updateCurrentRecord(DataParam param) {
		String currentId = param.get(idField);
		DataParam queryParam = new DataParam(idField,currentId);
		DataRow row = this.queryCurrentRecord(queryParam);
		String srcParentId = row.stringValue(parentIdField);
		String curParentId = param.get(parentIdField);
		if (!srcParentId.equals(curParentId)){
			String newMenuSort = String.valueOf(this.getNewMaxSort(curParentId));
			param.put(sortField,newMenuSort);
		}
		DataParam newParam = row.toDataParam(true);
		newParam.merge(param);
		String statementId = this.sqlNameSpace+"."+"updateTreeRecord";
		this.daoHelper.updateRecord(statementId, newParam);
	}	
}
