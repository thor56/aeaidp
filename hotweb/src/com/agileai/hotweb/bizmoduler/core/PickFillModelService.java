package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface PickFillModelService {
	List<DataRow> queryPickFillRecords(DataParam param);
}
