package com.agileai.hotweb.controller.core;

import java.io.File;

import com.agileai.domain.DataParam;
import com.agileai.util.DateUtil;

public class FileUploadHandler extends BaseHandler{
	protected DataParam resourceParam = new DataParam();
	protected String resourceRelativePath = null;
	
	public FileUploadHandler(){
		super();
	}
	
	protected File buildResourseSavePath(String columnId){
		File result = null;
		File contextFile = new File(System.getProperty("catalina.base")+"/webapps");
		String parentAbsolutePath = contextFile.getAbsolutePath();
		String resourseFilePath = parentAbsolutePath+File.separator+"HotServer"
				+File.separator+"reponsitory"+File.separator+"resourse"+File.separator +columnId
				+File.separator+DateUtil.getYear()+File.separator+DateUtil.getMonth()+File.separator+DateUtil.getDay();
		
		resourceRelativePath = "/HotServer"+"/reponsitory"+"/resourse/"+columnId
				+"/"+DateUtil.getYear()+"/"+DateUtil.getMonth()+"/"+DateUtil.getDay();
		result = new File(resourseFilePath);
		return result;
	}
}
