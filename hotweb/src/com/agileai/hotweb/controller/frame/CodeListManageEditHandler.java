package com.agileai.hotweb.controller.frame;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.controller.frame.CodeListManageListHandler.ManageType;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class CodeListManageEditHandler extends StandardEditHandler{
	public CodeListManageEditHandler(){
		super();
		this.listHandlerClass = CodeListManageListHandler.class;
		this.serviceId = "codeListService";
	}
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);
			String codeType = param.get("CODE_TYPE_ID");
			FormSelectFactory.cleanCache(codeType);
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}	
	public ViewRenderer doChangeTypeIdAction(DataParam param){
		String rspText = null;
		String codeTypeId = param.get("CODE_TYPE_ID");
		if (!StringUtil.isNullOrEmpty(codeTypeId)){
			StandardService service = (StandardService)this.lookupService("codeTypeService");
			DataParam dataParam = new DataParam();
			dataParam.put("TYPE_ID",codeTypeId);
			DataRow row = service.getRecord(dataParam);
			String charLimit = row.getString("CHARACTER_LIMIT");
			String lengthLimit = row.stringValue("LEGNTT_LIMIT");
			rspText = charLimit+","+lengthLimit;
		}
		return new AjaxRenderer(rspText);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("CODE_FLAG",FormSelectFactory.create("SYS_VALID_TYPE").addSelectedValue(getAttributeValue("CODE_FLAG","1")));
		setAttribute("CODE_SORT",getAttributeValue("CODE_SORT","1"));
		setAttribute(param,"manageType");
		setAttribute(param,"IS_EDITABLE");
		String manageType = param.get("manageType");
		if (manageType.equals(ManageType.BY_GROUP)){
			DataParam dataParam = new DataParam();
			dataParam.put("TYPE_GROUP", param.get("TYPE_GROUP"));
			String operaType = this.getOperaType();
			if (OperaType.CREATE.equals(operaType)){
				dataParam.put("IS_EDITABLE","Y");
			}
			FormSelect typeIdFormSelect = FormSelectFactory.create("util.getCodeType", dataParam);
			String typeId = param.get("CODE_TYPE_ID");
			if (StringUtil.isNullOrEmpty(typeId))
				typeId = param.get("TYPE_ID");
			typeIdFormSelect.setSelectedValue(typeId);
			setAttribute("TYPE_ID", typeIdFormSelect);
		}
		else if (manageType.equals(ManageType.BY_TYPE)){
			DataParam dataParam = new DataParam();
			String typeId = param.get("CODE_TYPE_ID");
			if (StringUtil.isNullOrEmpty(typeId))
				typeId = param.get("TYPE_ID");
			dataParam.put("TYPE_ID", typeId);
			String operaType = this.getOperaType();
			if (OperaType.CREATE.equals(operaType)){
				dataParam.put("IS_EDITABLE","Y");
			}
			FormSelect typeIdFormSelect = FormSelectFactory.create("util.getCodeType", dataParam);
			typeIdFormSelect.setSelectedValue(typeId);
			setAttribute("TYPE_ID", typeIdFormSelect);
		}
		else{
			String typeId = param.get("CODE_TYPE_ID");
			
			DataParam dataParam = new DataParam();
			String operaType = this.getOperaType();
			if (OperaType.CREATE.equals(operaType)){
				dataParam.put("IS_EDITABLE","Y");
			}
			if (StringUtil.isNullOrEmpty(typeId))
				typeId = param.get("TYPE_ID");
			setAttribute("TYPE_ID", FormSelectFactory.create("util.getCodeType",dataParam).addSelectedValue(typeId));
		}
		String codeTypeId = param.get("CODE_TYPE_ID");
		if (StringUtil.isNullOrEmpty(codeTypeId))
			codeTypeId = param.get("TYPE_ID");
		if (!StringUtil.isNullOrEmpty(codeTypeId)){
			StandardService service = (StandardService)this.lookupService("codeTypeService");
			DataParam dataParam = new DataParam();
			dataParam.put("TYPE_ID",codeTypeId);
			DataRow row = service.getRecord(dataParam);
			String charLimit = row.getString("CHARACTER_LIMIT");
			String lengthLimit = row.stringValue("LEGNTT_LIMIT");
			this.setAttribute("CHARACTER_LIMIT",charLimit);
			this.setAttribute("LEGNTT_LIMIT",lengthLimit);
		}
	}
}