package com.agileai.hotweb.common;

import java.util.HashMap;

public class GlobalCacheManager {
	public static HashMap<ClassLoader,HashMap<String,Object>> Container = new HashMap<ClassLoader,HashMap<String,Object>>();
	
	public synchronized static void putCacheValue(String key,Object value){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader instanceof ModuleClassLoader){
			classLoader = classLoader.getParent();
		}
		if (!Container.containsKey(classLoader)){
			HashMap<String,Object> cacheBlock = new HashMap<String,Object>();
			Container.put(classLoader, cacheBlock);
		}
		HashMap<String,Object> cacheBlock = Container.get(classLoader);
		cacheBlock.put(key, value);
	}
	
	public static Object getCacheValue(String key){
		Object result = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader instanceof ModuleClassLoader){
			classLoader = classLoader.getParent();
		}
		HashMap<String,Object> cacheBlock = Container.get(classLoader);
		if (cacheBlock != null){
			result = cacheBlock.get(key);
		}
		return result;
	}
	
	public static HashMap<String,Object> getCacheBlock(){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader instanceof ModuleClassLoader){
			classLoader = classLoader.getParent();
		}
		HashMap<String,Object> cacheBlock = Container.get(classLoader);
		return cacheBlock;
	}
	
	public static HashMap<String,Object> getCacheBlock(ClassLoader classLoader){
		HashMap<String,Object> cacheBlock = Container.get(classLoader);
		return cacheBlock;
	}
	
	public static void removeCache(ClassLoader classLoader){
		if (Container.containsKey(classLoader)){
			Container.remove(classLoader);
		}
	}
}