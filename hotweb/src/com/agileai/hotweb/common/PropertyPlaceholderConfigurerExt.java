package com.agileai.hotweb.common;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.agileai.util.CryptionUtil;

public class PropertyPlaceholderConfigurerExt extends PropertyPlaceholderConfigurer{
    @Override 
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) 
                    throws BeansException { 
		String secretKey = props.getProperty("dataSource.securityKey");
        String password = props.getProperty("dataSource.password"); 
        if (password != null) { 
                props.setProperty("dataSource.password", CryptionUtil.decryption(password, secretKey)); 
        } 
        super.processProperties(beanFactory, props); 
    } 
}