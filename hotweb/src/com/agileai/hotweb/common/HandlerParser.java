package com.agileai.hotweb.common;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;

public class HandlerParser {
	private static HashMap<ClassLoader,HandlerParser> handlerParserCache = new HashMap<ClassLoader,HandlerParser>();
	public static final String ModuleHandlerConfigFile = "HandlerModule.xml";
	
	private Map<String,String> classNameCache = new HashMap<String,String>();
	private Map<String,String> pageURLCache = new HashMap<String,String>();
	private Map<String,String> idReverseCache = new HashMap<String,String>();
	private ClassLoader classLoader = null;
	private Document document = null;
	
	private HandlerParser (){
	}
	
	public static HandlerParser getOnly(){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		HandlerParser instance = handlerParserCache.get(classLoader);
		if (instance == null){
			try {
				instance = (HandlerParser)classLoader.loadClass("com.agileai.hotweb.common.HandlerParser").newInstance();			
				handlerParserCache.put(classLoader, instance);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	public static HandlerParser getOnly(ClassLoader classLoader){
		HandlerParser instance = handlerParserCache.get(classLoader);
		if (instance == null){
			try {
				instance = (HandlerParser)classLoader.loadClass("com.agileai.hotweb.common.HandlerParser").newInstance();	
				instance.classLoader = classLoader;
				instance.initHandlerModule(ModuleHandlerConfigFile);
				handlerParserCache.put(classLoader, instance);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return instance;
	}	
	
	public static void init(String handlerConfigLocation){
		HandlerParser instance = getOnly();
		if (instance.document == null){
			InputStream configStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(handlerConfigLocation);				
			instance.document = XmlUtil.readDocument(configStream);
		}
	}
	
	public void initHandlerModule(String handlerConfigLocation){
		if (this.document == null){
			InputStream configStream = this.classLoader.getResourceAsStream(handlerConfigLocation);				
			this.document = XmlUtil.readDocument(configStream);
		}
	}
	
	public static void destroy(ClassLoader moduleClassLoader){
		if (handlerParserCache.containsKey(moduleClassLoader)){
			handlerParserCache.remove(moduleClassLoader);			
		}
	}
	
	@SuppressWarnings({ "rawtypes"})
	public BaseHandler instantiateHandler(String handlerId) throws Exception{
		ClassLoader classLoader = null;		
		if (this.classLoader != null){
			classLoader = this.classLoader;
		}else{
			classLoader = Thread.currentThread().getContextClassLoader();
		}
		String className = parseClassName(handlerId);
		Class handlerClass = classLoader.loadClass(className);
		BaseHandler handler =  (BaseHandler)handlerClass.newInstance();
		if (handler != null){
			if (this.classLoader == null){
				handler.setHandlerId(handlerId);				
			}
			handler.setClassLoader(classLoader);
	
			String displayPage = paresePageURL(handlerId);
			if (!StringUtil.isNullOrEmpty(displayPage)){
				handler.setPage(displayPage);				
			}
		}
		return handler;
	}

	public String parseClassName(String handlerId){
		String result = null;
		if (!classNameCache.containsKey(handlerId)){
			String handlerPath = "//beans/bean[@id='"+handlerId+"']";
			Element handlerElement = (Element)document.selectSingleNode(handlerPath);
			if (handlerElement != null){
				String className = handlerElement.attribute("class").getText();
				classNameCache.put(handlerId, className);				
			}else{
				System.err.println(handlerId +" Handler is not config , please check it !");
			}
		}
		result = classNameCache.get(handlerId);
		return result;
	}
	
	public String parseHandlerId(String handlerClazzName){
		String result = null;
		if (!idReverseCache.containsKey(handlerClazzName)){
			String handlerPath = "//beans/bean[@class='"+handlerClazzName+"']";
			Element handlerElement = (Element)document.selectSingleNode(handlerPath);
			if (handlerElement != null){
				String handlerId = handlerElement.attribute("id").getText();
				idReverseCache.put(handlerClazzName, handlerId);				
			}else{
				System.err.println(handlerClazzName +" handler is not config , please check it !");
			}
		}
		result = idReverseCache.get(handlerClazzName);
		return result;
	}
	
	public String paresePageURL(String handlerId){
		String result = null;
		if (!pageURLCache.containsKey(handlerId)){
			String handlerPath = "//beans/bean[@id='"+handlerId+"']";
			Element handlerElement = (Element)document.selectSingleNode(handlerPath);
			if (handlerElement != null){
				Attribute attribute = handlerElement.attribute("page");
				String pageURL = "";
				if (attribute != null){
					pageURL = attribute.getText();			
				}
				pageURLCache.put(handlerId, pageURL);
			}else{
				System.err.println(handlerId +" handler is not config , please check it !");
			}
		}
		result = pageURLCache.get(handlerId);
		return result;
	}
}
