package com.agileai.hotweb.common;

import java.util.HashMap;

public class MemSecrityKeys {
	private static HashMap<String,String> AppSecKeyPairs = new HashMap<String,String>();
	
	public static void setSecrityKeyPair(String appName,String randomKey){
		AppSecKeyPairs.put(appName, randomKey);
	}
	
	public static String getSecrityKey(String appName){
		return AppSecKeyPairs.get(appName);
	}
}
